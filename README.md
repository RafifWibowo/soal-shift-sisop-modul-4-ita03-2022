# soal-shift-sisop-modul-4-ITA03-2022

Laporan pengerjaan soal shift modul 4 Praktikum Sistem Operasi 2022 Kelompok ITA03

## Anggota Kelompok:

1. Salsabila Briliana Ananda Sofi - 5027201003
2. Rafif Naufaldi Wibowo - \_027201010
3. Fairuz Azka Maulana - \_027201017

## Source Code

---

[Code](./anya_ita03.c)

## Analisa Soal

---

Kita diminta untuk mengimplementasikan FUSE yang sudah kita pelajari di modul 4 dimana saat program dijalankan akan mengubah nama direktori dan file pada kondisi tertentu. Perubahan yang terjadi adalah tiap direktori dan file yang sesuai kondisi, akan di encode menggunakan algoritma `Atbash Chiper`, `rot13`, dan `Vignere Chiper`.

Tedapat beberapa hal yang perlu diperhatikan:

1. Mount source file berada pada direktori `/home/[USER]/Documents`.
2. semua soal dikerjakan dalam satu file c yang sama dengan nama file `anya_[kelompok].c`.

## Soal 1

---

## Study case soal 1

---

Anya adalah adalah seorang programmer wibu yang suka mengoleksi anime. Dia sangat senang membuat program yang dikolaborasikan dengan anime. Suatu hari, Anya sedang ingin membuat system dengan ketentuan:

- Semua direktori dengan awalan `Animeku_` akan terencode dengan ketentuan semua file yang terdapat huruf besar akan ter encode dengan atbash cipher dan huruf kecil akan terencode dengan rot13

  ```sh
  Contoh :
  “Animeku_/anya_FORGER.txt” → “Animeku_/naln_ULITVI.txt”
  ```

- Semua direktori di-rename dengan awalan `Animeku_`, maka direktori tersebut akan menjadi direktori ter-encode dengan ketentuan sama dengan 1a.
- Apabila direktori yang terenkripsi di-rename menjadi tidak ter-encode, maka isi direktori tersebut akan terdecode.
- Setiap data yang terencode akan masuk dalam file `Wibu.log`.

  ```sh
  Contoh isi:
  RENAME terenkripsi /home/[USER]/Downloads/hai --> /home/[USER]/Downloads/Animeku_hebat
  RENAME terdecode /home/[USER]/Downloads/Animeku_ --> /home/[USER]/Downloads/Coba
  ```

- Metode encode pada suatu direktori juga berlaku terhadap direktori yang ada di dalamnya.(rekursif)

Note:
filesystem berfungsi normal layaknya linux pada umumnya, Mount source (root) filesystem adalah directory /home/[USER]/Documents, dalam penamaan file `/` diabaikan, dan ekstensi tidak perlu di-encode

## 1.A

---

## Analisis soal

---

Pada poin A kami diminta membuat direktori dengan awalan “Animeku\_”, direktori ini akan terencode dengan ketentuan yaitu semua file yang terdapat huruf besar akan ter-encode dengan atbash cipher dan untuk huruf kecil akan ter-encode dengan rot13.

## Cara Pengerjaan

---

Pertama kita mengambil template dari modul 4 untuk mengerjakan soal ini.

Kemudian kita akan membuat fungsi untuk melakukan algoritma `Atbash Chiper` dan `rot13`.

```c
char upper_case[] = {'Z', 'Y', 'X', 'W', 'V', 'U',
                     'T', 'S', 'R', 'Q', 'P', 'O',
                     'N', 'M', 'L', 'K', 'J', 'I',
                     'H', 'G', 'F', 'E', 'D', 'C', 'B', 'A'};

char lower_case[] = {'a', 'b', 'c', 'd', 'e', 'f',
                     'g', 'h', 'i', 'j', 'k', 'l',
                     'm', 'n', 'o', 'p', 'q', 'r',
                     's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};

// melakukan encode/decode
char *atRotEncryption(char src[])
{
    char str[maxSize];
    strcpy(str, src);
    int ascii;
    for (int i = 0; i < strlen(str); i++)
    {
        ascii = str[i];
        if (ascii >= 'A' && ascii <= 'Z')
            str[i] = upper_case[ascii - 65];
        if (ascii >= 'a' && ascii <= 'z')
        {
            if (ascii >= 'n' && ascii <= 'z')
            {
                str[i] = lower_case[(ascii - 13) - 97];
            }
            if (ascii >= 'a' && ascii <= 'm')
            {
                str[i] = lower_case[(ascii + 13) - 97];
            }
        }
    }
    char *res = str;
    return res;
}
```

Setelah itu kita akan membuat suatu fungsi agar saat kita mount FUSE nya tetap dapat membaca nama file yang sudah ter-encode.

```c
char *find_path(const char *path)
{
    char fpath[2 * maxSize];
    bool flag, toAtRot = 0, toVignere = 0;

    char *strAtRot;
    if (strcmp(path, "/"))
    {
        strAtRot = strstr(path, "/Animeku_");
        if (strAtRot)
        {
            toAtRot = 1;
            strAtRot++;
        }
    }

    ....
    ....

    if (strcmp(path, "/") == 0)
    {
        sprintf(fpath, "%s", dirpath);
    }
    else if (toAtRot)
    {
        char pathOrigin[maxSize] = "";
        char t[maxSize];

        strncpy(pathOrigin, path, strlen(path) - strlen(strAtRot));
        strcpy(t, strAtRot);

        char *selectedFile;
        char *rest = t;

        flag = 0;

        while ((selectedFile = strtok_r(rest, "/", &rest)))
        {
            if (!flag)
            {
                strcat(pathOrigin, selectedFile);
                flag = 1;
                continue;
            }

            char checkType[2 * maxSize];
            sprintf(checkType, "%s/%s", pathOrigin, selectedFile);
            strcat(pathOrigin, "/");

            if (strlen(checkType) == strlen(path))
            {
                char pathFolder[2 * maxSize];
                sprintf(pathFolder, "%s%s%s", dirpath, pathOrigin, selectedFile);

                DIR *dp = opendir(pathFolder);
                if (!dp)
                {
                    char *ext;
                    ext = strrchr(selectedFile, '.');

                    char fileName[maxSize] = "";
                    if (ext)
                    {
                        strncpy(fileName, selectedFile, strlen(selectedFile) - strlen(ext));
                        sprintf(fileName, "%s%s", atRotEncryption(fileName), ext);
                    }
                    else if (toAtRot)
                        strcpy(fileName, atRotEncryption(selectedFile));

                    printf("%s\n", fileName);
                    strcat(pathOrigin, fileName);
                }
                else
                    strcat(pathOrigin, atRotEncryption(selectedFile));
            }
            else
            {
                strcat(pathOrigin, atRotEncryption(selectedFile));
            }
        }

        sprintf(fpath, "%s%s", dirpath, pathOrigin);
    }

    ....
    ....

    else
        sprintf(fpath, "%s%s", dirpath, path);

    char *fpath_to_return = fpath;
    return fpath_to_return;
}
```

Setelah itu kita mengedit xmp_readdir agar bisa menjalankan program sesuai dengan soal.

```c
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[maxSize];
    bool toAtRot = strstr(path, "/Animeku_");

    ....
    ....

    strcpy(fpath, find_path(path));

    int res = 0;
    DIR *dp;
    struct dirent *de;

    (void)offset;
    (void)fi;

    dp = opendir(fpath);
    if (dp == NULL)
        return -errno;

    while ((de = readdir(dp)) != NULL)
    {
        struct stat st;
        memset(&st, 0, sizeof(st));
        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        if (strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0)
        {
            res = (filler(buf, de->d_name, &st, 0));
        }
        else if (toAtRot)
        {
            if (de->d_type & DT_DIR)
            {
                char temp[maxSize];
                strcpy(temp, de->d_name);

                res = (filler(buf, atRotEncryption(temp), &st, 0));
            }
            else
            {
                char *ext;
                ext = strrchr(de->d_name, '.'); // abc.112.1

                char fileName[maxSize] = "";
                if (ext)
                {
                    strncpy(fileName, de->d_name, strlen(de->d_name) - strlen(ext));
                    strcpy(fileName, atRotEncryption(fileName)); // encode --> edocne.log
                    strcat(fileName, ext);
                }
                else
                {
                    strcpy(fileName, atRotEncryption(de->d_name));
                }
                res = (filler(buf, fileName, &st, 0));
            }
        }

        ....
        ....

        else
            res = (filler(buf, de->d_name, &st, 0));

        if (res != 0)
            break;
    }
    closedir(dp);
    return 0;
}
```

## 1.B

---

## Analisis soal

---

Pada poin B untuk semua direktori yang di-rename dengan awalan “Animeku\_”, maka direktori tersebut akan menjadi direktori yang ter-encode dengan ketentuan sama dengan 1A.

## Cara Pengerjaan

---

Kami masih belum dapat mengerjakan poin ini

## 1.C

---

## Analisis soal

---

Pada poin C untuk direktori yang telah terenkripsi di-rename menjadi tidak ter-encode atau tidak dengan awalan "Animeku\_", maka isi dari direktori tersebut akan terdecode.

## Cara Pengerjaan

---

Kami masih belum dapat mengerjakan poin ini

## 1.D

---

## Analisis soal

---

Pada poin D kami diminta untuk mencatat setiap data yang ter-encode ke dalam file “Wibu.log”.

## Cara Pengerjaan

---

Kami masih belum dapat mengerjakan poin ini

## 1.E

---

## Analisis soal

---

Pada poin E ini yaitu metode encode pada suatu direktori juga berlaku terhadap direktori yang ada di dalamnya (rekursif). Artinya direktori didalamnya juga akan diencode dengan ketentuan seperti pada poin A.

## Cara Pengerjaan

---

Kami masih belum dapat mengerjakan poin ini

## Screenshot hasil program dijalankan

---

Berikut adalah hasil screenshot saat fuse sudah di mount
<br>
![foto](./img/ss1.png)

## Soal 2

---

## 2.a

---

## Analisa

---

Setiap direktori yang dibuat dengan nama `/IAN_`, seluruh isi dari direktori tersebut akan di encode menggunakan algoritma `Vignere Chiper`.

## Cara Pengerjaan

---

Kami masih belum dapat mengerjakan poin ini

## 2.b

---

## Analisa

---

Bila kita merename suatu direktori menjadi sesuai dengan nomer 2.a, maka seluruh isi dari direktori tersebut akan di encode dengan algoritma `Vignere Chiper`.

## Cara Pengerjaan

---

Kami masih belum dapat mengerjakan poin ini

## 2.c

---

## Analisa

---

Kebalikan dari soal nomor 2.b, bila unsur yang terdapat pada nomor 2.a dihilangkan, maka seluruh isi dari direktori tersebut akan ter-decode seperti sedia kala.

## Cara Pengerjaan

---

Kami masih belum dapat mengerjakan poin ini

## 2.d

---

## Analisa

---

Pada poin ini, kami diminta untuk membuat log system untuk memantau aktivitas kita pada file system ini. Log akan menyimpan semua daftar perintah system call yang telah dijalankan pada file system.

## Cara Pengerjaan

---

Kami masih belum dapat mengerjakan poin ini

## 2.e

---

## Analisa

---

Pada poin ini, log system akan dibagi menjadi 2 level, yaitu **INFO** dan **WARNING**. **WARNING** digunakan unutk mencatat system call rmdir dan unlink. Sisanya akan dicatat dengan level **INFO**.

## Cara Pengerjaan

---

Kami masih belum dapat mengerjakan poin ini

## Revisi

---

Pada saat pengerjaan kami baru mengerjakan soal nomor 1.a. Itupun masih belum dapat membaca hasil mountnya. Tetapi sekarang sudah bisa
