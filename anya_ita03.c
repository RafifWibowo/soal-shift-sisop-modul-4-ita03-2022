#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <stdbool.h>
#include <sys/time.h>
#define maxSize 1024
char dirpath[maxSize];

char upper_case[] = {'Z', 'Y', 'X', 'W', 'V', 'U',
                     'T', 'S', 'R', 'Q', 'P', 'O',
                     'N', 'M', 'L', 'K', 'J', 'I',
                     'H', 'G', 'F', 'E', 'D', 'C', 'B', 'A'};

char lower_case[] = {'a', 'b', 'c', 'd', 'e', 'f',
                     'g', 'h', 'i', 'j', 'k', 'l',
                     'm', 'n', 'o', 'p', 'q', 'r',
                     's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};

// melakukan encode/decode
char *atRotEncryption(char src[])
{
    char str[maxSize];
    strcpy(str, src);
    int ascii;
    for (int i = 0; i < strlen(str); i++)
    {
        ascii = str[i];
        if (ascii >= 'A' && ascii <= 'Z')
            str[i] = upper_case[ascii - 65];
        if (ascii >= 'a' && ascii <= 'z')
        {
            if (ascii >= 'n' && ascii <= 'z')
            {
                str[i] = lower_case[(ascii - 13) - 97];
            }
            if (ascii >= 'a' && ascii <= 'm')
            {
                str[i] = lower_case[(ascii + 13) - 97];
            }
        }
    }
    char *res = str;
    return res;
}

char *vignere_encrypt(char src[])
{
    char msg[maxSize];
    strcpy(msg, src);
    char key[] = "INNUGANTENG";
    int msgLen = strlen(msg);
    int keyLen = strlen(key);

    char newKey[msgLen], encMsg[msgLen];

    int i, j;
    for (i = 0, j = 0; i < msgLen; ++i, ++j)
    {
        if (j == keyLen)
            j = 0;

        newKey[i] = key[j];
    }
    newKey[i] = '\0';

    for (i = 0; i < msgLen; ++i)
    {
        encMsg[i] = ((msg[i] + newKey[i]) % 26) + 'A';
    }
    encMsg[i] = '\0';

    char *res = encMsg;
    return res;
}

char *vignere_decrypt(char src[])
{
    char msg[maxSize];
    strcpy(msg, src);
    char key[] = "INNUGANTENG";
    int msgLen = strlen(msg);
    int keyLen = strlen(key);

    char newKey[msgLen], decMsg[msgLen];

    int i, j;
    for (i = 0, j = 0; i < msgLen; ++i, ++j)
    {
        if (j == keyLen)
            j = 0;

        newKey[i] = key[j];
    }
    newKey[i] = '\0';

    for (i = 0; i < msgLen; ++i)
    {
        decMsg[i] = (((msg[i] - newKey[i]) + 26) % 26) + 'A';
    }
    decMsg[i] = '\0';

    char *res = decMsg;
    return res;
}

char *find_path(const char *path)
{
    char fpath[2 * maxSize];
    bool flag, toAtRot = 0, toVignere = 0;

    char *strAtRot;
    if (strcmp(path, "/"))
    {
        strAtRot = strstr(path, "/Animeku_");
        if (strAtRot)
        {
            toAtRot = 1;
            strAtRot++;
        }
    }
    char *strVignere;
    if (strcmp(path, "/"))
    {
        strVignere = strstr(path, "/IAN_");
        if (strVignere)
        {
            toVignere = 1;
            strVignere++;
        }
    }

    if (strcmp(path, "/") == 0)
    {
        sprintf(fpath, "%s", dirpath);
    }
    else if (toAtRot)
    {
        char pathOrigin[maxSize] = "";
        char t[maxSize];

        strncpy(pathOrigin, path, strlen(path) - strlen(strAtRot));
        strcpy(t, strAtRot);

        char *selectedFile;
        char *rest = t;

        flag = 0;

        while ((selectedFile = strtok_r(rest, "/", &rest)))
        {
            if (!flag)
            {
                strcat(pathOrigin, selectedFile);
                flag = 1;
                continue;
            }

            char checkType[2 * maxSize];
            sprintf(checkType, "%s/%s", pathOrigin, selectedFile);
            strcat(pathOrigin, "/");

            if (strlen(checkType) == strlen(path))
            {
                char pathFolder[2 * maxSize];
                sprintf(pathFolder, "%s%s%s", dirpath, pathOrigin, selectedFile);

                DIR *dp = opendir(pathFolder);
                if (!dp)
                {
                    char *ext;
                    ext = strrchr(selectedFile, '.');

                    char fileName[maxSize] = "";
                    if (ext)
                    {
                        strncpy(fileName, selectedFile, strlen(selectedFile) - strlen(ext));
                        sprintf(fileName, "%s%s", atRotEncryption(fileName), ext);
                    }
                    else if (toAtRot)
                        strcpy(fileName, atRotEncryption(selectedFile));

                    printf("%s\n", fileName);
                    strcat(pathOrigin, fileName);
                }
                else
                    strcat(pathOrigin, atRotEncryption(selectedFile));
            }
            else
            {
                strcat(pathOrigin, atRotEncryption(selectedFile));
            }
        }

        sprintf(fpath, "%s%s", dirpath, pathOrigin);
    }
    else if (toVignere)
    {
        char pathOrigin[maxSize] = "";
        char t[maxSize];

        strncpy(pathOrigin, path, strlen(path) - strlen(strVignere));
        strcpy(t, strVignere);

        char *selectedFile;
        char *rest = t;

        flag = 0;

        while ((selectedFile = strtok_r(rest, "/", &rest)))
        {
            if (!flag)
            {
                strcat(pathOrigin, selectedFile);
                flag = 1;
                continue;
            }

            char checkType[2 * maxSize];
            sprintf(checkType, "%s/%s", pathOrigin, selectedFile);
            strcat(pathOrigin, "/");

            if (strlen(checkType) == strlen(path))
            {
                char pathFolder[2 * maxSize];
                sprintf(pathFolder, "%s%s%s", dirpath, pathOrigin, selectedFile);

                DIR *dp = opendir(pathFolder);
                if (!dp)
                {
                    char *ext;
                    ext = strrchr(selectedFile, '.');

                    char fileName[maxSize] = "";
                    if (ext)
                    {
                        strncpy(fileName, selectedFile, strlen(selectedFile) - strlen(ext));
                        sprintf(fileName, "%s%s", vignere_encrypt(fileName), ext);
                    }
                    else if (toVignere)
                        strcpy(fileName, vignere_encrypt(selectedFile));

                    printf("%s\n", fileName);
                    strcat(pathOrigin, fileName);
                }
                else
                    strcat(pathOrigin, vignere_encrypt(selectedFile));
            }
            else
            {
                strcat(pathOrigin, vignere_encrypt(selectedFile));
            }
        }

        sprintf(fpath, "%s%s", dirpath, pathOrigin);
    }
    else
        sprintf(fpath, "%s%s", dirpath, path);

    char *fpath_to_return = fpath;
    return fpath_to_return;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[maxSize];
    bool toAtRot = strstr(path, "/Animeku_");
    bool toVignere = strstr(path, "/IAN_");
    strcpy(fpath, find_path(path));

    int res = 0;
    DIR *dp;
    struct dirent *de;

    (void)offset;
    (void)fi;

    dp = opendir(fpath);
    if (dp == NULL)
        return -errno;

    while ((de = readdir(dp)) != NULL)
    {
        struct stat st;
        memset(&st, 0, sizeof(st));
        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        if (strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0)
        {
            res = (filler(buf, de->d_name, &st, 0));
        }
        else if (toAtRot)
        {
            if (de->d_type & DT_DIR)
            {
                char temp[maxSize];
                strcpy(temp, de->d_name);

                res = (filler(buf, atRotEncryption(temp), &st, 0));
            }
            else
            {
                char *ext;
                ext = strrchr(de->d_name, '.'); // abc.112.1

                char fileName[maxSize] = "";
                if (ext)
                {
                    strncpy(fileName, de->d_name, strlen(de->d_name) - strlen(ext));
                    strcpy(fileName, atRotEncryption(fileName)); // encode --> edocne.log
                    strcat(fileName, ext);
                }
                else
                {
                    strcpy(fileName, atRotEncryption(de->d_name));
                }
                res = (filler(buf, fileName, &st, 0));
            }
        }
        else if (toVignere)
        {
            if (de->d_type & DT_DIR)
            {
                char temp[maxSize];
                strcpy(temp, de->d_name);

                res = (filler(buf, vignere_encrypt(temp), &st, 0));
            }
            else
            {
                char *ext;
                ext = strrchr(de->d_name, '.'); // abc.112.1

                char fileName[maxSize] = "";
                if (ext)
                {
                    strncpy(fileName, de->d_name, strlen(de->d_name) - strlen(ext));
                    strcpy(fileName, vignere_encrypt(fileName)); // encode --> edocne.log
                    strcat(fileName, ext);
                }
                else
                {
                    strcpy(fileName, vignere_encrypt(de->d_name));
                }
                res = (filler(buf, fileName, &st, 0));
            }
        }
        else
            res = (filler(buf, de->d_name, &st, 0));

        if (res != 0)
            break;
    }
    closedir(dp);
    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[maxSize];
    strcpy(fpath, find_path(path));

    int fd;
    int res;

    (void)fi;
    fd = open(fpath, O_RDONLY);
    if (fd == -1)
        return -errno;

    res = pread(fd, buf, size, offset);
    if (res == -1)
        res = -errno;

    close(fd);
    return res;
}

static int xmp_getattr(const char *path, struct stat *stbuf)
{
    char fpath[maxSize];
    strcpy(fpath, find_path(path));

    int res;
    res = lstat(fpath, stbuf);
    if (res == -1)
        return -errno;

    return 0;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
};

int main(int argc, char *argv[])
{
    char *username = getenv("USER");

    sprintf(dirpath, "/home/%s/Documents", username);

    umask(0);

    return fuse_main(argc, argv, &xmp_oper, NULL);
}
